# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Project ###

Early in 2017, I was looking to expand my knowledge of LINUX, GIT and Python.  I was also interested in the maker craze that was sweeping the
nation.  So I did what any red-blooded nerd would do and I decided that to cook up a completely friviolous project that would tickle those fancies.
So here I am.  This project has the following components:
	
##### Woodworking #####

- Draw up plans in sketchup that gave me dimensions for a small enviormental monitor station.
- Using pine, rip lumber down to correct measurements and assemble all pieces using wood glue and pin nails.
- Use varnish to finish the wood.
	
##### Maker #####

- Install a raspi zero w+ into the afrementioned enviormental monitor station using rivits as posts.
- Solder and connect:
  * LCD Screen to output readings and errors
  * Small buttion to change screens and perform clean shutdown.
  * Humidity and Temperature sensor.

##### Coding #####

- Write a program based in Python that uses the GPIO pins to communicate the aforementioned components.
- Write a bash script that runs the python script ever 60 minutes using a cron job.
- Report collected data to a Influx db instance
- Graph that time-series data with a clean, crip Grafana graph.
	
### How do I get set up? ###

* Clone Repo into a project folder
* Install Python 2.7 and PIP
  * PIP install
    * ```sudo apt install python-pip```
    * ```python -m pip install --upgrade pip setuptools wheel```
  * Install Modules

     ```
     sudo pip install --force-pi adafruit_python_dht
     cd Adafruit_Python_DHT/ && sudo python setup.py install --force-pi
     sudo pip install influxdb socket meteocalc RPLCD


     ```


  
    
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact