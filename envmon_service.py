import time
import ccond_forecast
import collect_reading
import lcd_write
import schedule
import threading
import newbutton


if __name__ == "__main__":
    thread = threading.Thread(target=newbutton.button_sense)
    thread.start()

    # Forecast update
    ccond_forecast.get_forcast()
    schedule.every(6).hours.do(ccond_forecast.get_forcast)

    # Room reading
    collect_reading.collect_reading()
    schedule.every(10).minutes.do(collect_reading.collect_reading)

    # CConds update
    ccond_forecast.get_current_conditions()
    schedule.every().hour.do(ccond_forecast.get_current_conditions)

    # Forecast display interval
    schedule.every(5).minutes.at(":00").do(lcd_write.display_forecast)

    # Current conditions Display interval
    schedule.every(3).minutes.at(":00").do(lcd_write.display_current)

    # Reading display interval
    schedule.every().minute.at(":00").do(lcd_write.display_reading)


    while True:
        schedule.run_pending()

        time.sleep(1)
