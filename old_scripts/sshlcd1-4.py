#!/usr/bin/python
#Tom Ivy 4/21/17
#Python 2.7

import sys
import socket
import Adafruit_DHT #Adafruit Sensor 
from influxdb import InfluxDBClient #influxdb client (read and write to influxDB)
import socket #to obtain hostname and IP Address
from time import sleep
import datetime #to print date and time
from meteocalc import Temp, heat_index #to calculate heat index
import RPLCD.gpio as rplcd  #to communicate with LCD SCREEN
from RPi import GPIO
import time


##Inputs##

frequency = 1 # minutes
frequency = frequency * 60


#Define LCD Screen
lcd = rplcd.CharLCD(cols=16, rows=2, pin_rs=37, pin_e=35, pins_data=[33, 31, 29, 23], numbering_mode=GPIO.BOARD)

#Define arguments for Adafruit_DHT function instead of command line parse
sensor =  Adafruit_DHT.AM2302
pin = '4'

#Gonna go GLOBAL with these variables to pass between functions cuz I don't know classes yet =(  Assining a value of None for if statement later
humidity = None
temperature = None
hi = None

def grab_temp():

	#Using Globals so we can pass these easily to lcdwrite() & influx_write()
	global humidity
	global temperature
	global hi
	global last_read

	#attempts to grab a sensor reading.  Uses the read retry method 15 times
	humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)

	if humidity is not None and temperature is not None:
		temperature = temperature * 9/5.0 + 32	 # convert temp to Fahrenheit

		if temperature >= 80:
	
			hi = heat_index(temperature, humidity)  # calls the meteocalc function to return heat index.
		else:
			hi = temperature  # Heat index is only valid on temps above 80 degrees F

		last_read = time.strftime("%I:%M")

def lcdwrite(humidity, temperature, hi):
	
	
	if humidity is not None and temperature is not None:
		lcd.cursor_pos = (0, 0)
		lcd.write_string("Temp:%dF" % temperature)
		lcd.cursor_pos = (1, 2)
		lcd.write_string("H:%d%%" % humidity)
		lcd.cursor_pos = (0, 10)
		lcd.write_string("HI:%dF" % hi)
        lcd.cursor_pos = (1, )
		lcd.write_string("LR:{}".format(last_read))

	else:
		lcd.cursor_pos = (0, 0)
		lcd.write_string(" FAILED READING")
		lcd.cursor_pos = (1, 0)
		lcd.write_string(" Call for help!")
		print ('failed')


def influx_write(humidity, temperature, hi):
	client = InfluxDBClient(
			host = '192.168.1.105',
			port = 8086,
			username = 'root',
			password = 'root',
			database = 'sensordata',
	)

	json_body = [
		{
		"measurement": socket.gethostname(),
		"tags": {
			"host": socket.gethostname()
		},
		"fields": {
			"temp": temperature,
			"Humidity": humidity,
			"Heat Index": hi
		}
		}
	]

	try:
		client.write_points(json_body)


	except Exception as e:
		lcd.clear()
		lcd.cursor_pos = (0, 1)
		lcd.write_string("DATABASE WRITE")
		lcd.cursor_pos = (1, 0)
		lcd.write_string("FAILURE....HELP!")
		print("Write points: {0}".format(json_body))

		print(e)

def collect_temp():
	grab_temp()
	lcdwrite(humidity, temperature, hi)
	influx_write(humidity, temperature, hi)

next_run_time = time.time() + frequency

while True:
	now = time.time()
	if now >= next_run_time:
		collect_temp()
		next_run_time += now + frequency

	else:
		sleep(1)



