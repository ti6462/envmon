import RPLCD.gpio
from RPi import GPIO
import RPLCD.gpio as rplcd
import datetime
import time
import collect_reading
import ccond_forecast



# LCD Setup
lcd_rows = 2
lcd_num_columns = 16
lcd = rplcd.CharLCD(cols=lcd_num_columns, rows=lcd_rows, pin_rs=37, pin_e=35,
                         pins_data=[33, 31, 29, 23], numbering_mode=GPIO.BOARD)

num_screens = 2


# Current Conditions
cur_cond_refresh_rate = 5
cur_cond_cycles = 1

#Forecast
forecast_refresh_rate = 5
forecast_cycles = 1


# Helper Functions

def write_framebuffer_to_lcd(framebuffer):
    """Write the framebuffer out to the specified LCD."""
    lcd.home()
    for row in framebuffer:
        lcd.write_string(row.ljust(lcd_num_columns)[:lcd_num_columns])
        lcd.write_string('\r\n')

def loop_string( string, framebuffer, row, delay=0.2):
    # Slowly scroll text on the lcd screen. Needs frame buffer
    padding = ' ' * lcd_num_columns
    s = padding + string + padding
    for i in range(len(s) - lcd_num_columns + 1):
        framebuffer[row] = s[i:i+lcd_num_columns]
        write_framebuffer_to_lcd(framebuffer)
        time.sleep(delay)

def write_to_lcd(cursor_pos, string):
    lcd.cursor_pos = cursor_pos
    lcd.write_string(string)

def boot_message():
    framebuffer_msg = [
        "   Hello, Tom   ",
        "   Booting up   "
    ]
    write_framebuffer_to_lcd(framebuffer_msg)


# Display Functions

def display_reading():
    reading = collect_reading.last_reading
    lcd.clear()

    if reading['rh'] is not None and reading['temp'] is not None:
        write_to_lcd((0, 0), "Temp:%dF" % reading['temp'])
        write_to_lcd((1, 0), "RH:%d%%" % reading['rh'])
        write_to_lcd((0, 10), "HI:%dF" % reading['hi'])
        write_to_lcd((1, 9), "{}".format(time.strftime("%I:%M%p")))

    else:
        write_to_lcd((0, 0), "  Indoor Sensor ")
        write_to_lcd((1, 0), "   No Reading   ")


def display_current():
    weather = ccond_forecast.current_conds
    lcd.clear()

    # [(coordinates), msg]
    messages = [
        [(1, 0), " Temp:{}F RH:{} ".format(weather['temp'], weather['humidity'])], #16 chars
        [(1, 0), "{}".format(weather['description'][0:15])],
        [(1, 0), "Wind: {}MPH {}". format(weather['wind_spd'], weather['wind_dir'])],
    ]
    if weather['snow'] > 0:
        messages.append([(1, 0), "Snow Rate: {}/hr".format(weather['snow'])])
    if weather['precip'] > 0:
        messages.append([(1, 0), "Percip Rate: {}/h".format(weather['precip'])])

    cycles = 0
    while cycles < cur_cond_cycles:
        for coordinates, message in messages:
            lcd.clear()
            write_to_lcd((0, 0), "OutDr Cond:{}".format(time.strftime("%m/%d")))
            write_to_lcd(coordinates, message)

            time.sleep(cur_cond_refresh_rate)
        cycles += 1


def display_forecast():
    forecast = ccond_forecast.fore_conds
    messages = [
        [(1, 0), "Hi {}F Low {}F".format(forecast['high_temp'], forecast['low_temp'])],
        [(1, 0), "{}".format(forecast['description'][0:15])],
        [(1, 0), "  Humidity {}%  ".format(forecast['humidity'])],
        [(1, 0), "Wind: {}MPH {}".format(forecast['wind_speed'], forecast['wind_cdir'])]
    ]

    if forecast['snow'] > 0:
        messages.append([(1,0), "Snow Chance {}%".format(forecast['pop'])])
        messages.append([(1,0), "Snow Acc {}in".format(round(forecast['snow'], 3))])

    if forecast['precip'] > 0:
        messages.append([(1,0), "Rain Chance {}%".format(forecast['pop'])])
        messages.append([(1,0), "Rain Acc {}in".format(round(forecast['precip'], 3))])

    if forecast['wind_gust'] >= 30:
        messages.append([(1,0), "WIND GUST {}MPH".format(forecast['wind_gust'])])

    cycles = 0
    while cycles < forecast_cycles:
        for coordinates, message in messages:
            lcd.clear()
            write_to_lcd((0, 0), "Tmrw Fcst:{}".format(forecast['date']))
            write_to_lcd(coordinates, message)

            time.sleep(forecast_refresh_rate)
        cycles += 1

def cycle_screens(screen_num):
    screens = [display_current, display_forecast]
    screens[screen_num]()
    display_current()














