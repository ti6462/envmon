#python 2.7

import sys
import datetime
import time
import RPi.GPIO as GPIO
import os
import lcd_write

GPIO.setmode(GPIO.BOARD)
GPIO.setup(11, GPIO.IN, pull_up_down=GPIO.PUD_UP)
last_screen_displayed = 0

def button_push(channel):
    global start
    global end
    global last_screen_displayed

    if GPIO.input(11) == 0:
        end = datetime.datetime.now()
        last_button_push_time = time.time()

    if GPIO.input(11) == 1:
        start = datetime.datetime.now()
        elapsed = start - end
        elapsed = elapsed.total_seconds()


        if elapsed >= 5:
            lcd_write.write_to_lcd((1, 0), "Halting System")
            os.system("sudo shutdown -h now")\

        elif elapsed < 5:
            lcd_write.cycle_screens(last_screen_displayed)
            last_screen_displayed += 1

            if last_screen_displayed == 2:
                last_screen_displayed = 0

def button_sense():
    while True:
        GPIO.add_event_detect(11, GPIO.BOTH, callback=button_push, bouncetime=100)

        time.sleep(1)


