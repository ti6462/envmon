import Adafruit_DHT
from influxdb import InfluxDBClient
import socket
from meteocalc import Temp, heat_index
import time


sensor = Adafruit_DHT.AM2302
pin = '4'

last_reading = {
    "rh": None,
    "temp": None,
    "hi": None,
    "read_time": None
}



def collect_reading():

    # attempts to grab a sensor reading.  Uses the read retry method 15 times
    humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)

    if humidity is not None and temperature is not None:
        last_reading['temp'] = temperature * 9/ 5.0 + 32  # convert temp to Fahrenheit
        last_reading['rh'] = humidity

        if temperature >= 80:

            last_reading['hi'] = heat_index(temperature, humidity)  # calls the meteocalc function to return heat index.
        else:
            last_reading['hi'] = temperature  # Heat index is only valid on temps above 80 degrees

        #last_reading['read_time'] = time.strftime("%I:%M%p")
        last_reading['read_time'] = time.time()

        influx_write()


def influx_write():
    client = InfluxDBClient(
        host='192.168.1.105',
        port=8086,
        username='root',
        password='root',
        database='sensordata',
    )

    json_body = [
        {
            "measurement": socket.gethostname(),
            "tags": {
                "host": socket.gethostname()
            },
            "fields": {
                "temp": last_reading['temp'],
                "Humidity": last_reading['rh'],
                "Heat Index": last_reading['hi']
            }
        }
    ]

    try:
        client.write_points(json_body)

    except Exception as e:
        raise
