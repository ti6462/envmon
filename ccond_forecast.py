import json
import requests
import pprint
import datetime
import time

api_key = '1933cd653d1b4ea899c8a2bb89f14ead'
base_url = "http://api.weatherbit.io/v2.0/"
city = "Weymouth"
country = "US"
current_conds = {
    "description": None,
    "temp": 0,
    "humidity": 0,
    "precip": 0,
    "snow": 0,
    "wind_dir": 0,
    "wind_spd": 0,
    "last_call": None
}
fore_conds = {
    "date": None,
    "high_temp": 0,
    "low_temp": 0,
    "description": None,
    "snow": 0,
    "precip": None,
    "pop": None,
    "humidity": None,
    "wind_gust": None,
    "wind_speed": None,
    "wind_cdir": None,
    "last_call": None
}

def get_current_conditions():

    url = "https://api.weatherbit.io/v2.0/current?city=Weymouth&country=US&state=MA&units=I&" \
          "key=1933cd653d1b4ea899c8a2bb89f14ead"

    response = requests.get(url)
    data = json.loads(response.content)
    data = data["data"][0]
    current_conds['description'] = data["weather"]["description"]
    current_conds['temp'] = data["temp"]
    current_conds['humidity'] = data["rh"]
    current_conds['precip'] = data["precip"]
    current_conds['snow'] = data["snow"]
    current_conds['wind_dir'] = data["wind_cdir"]
    current_conds['wind_spd'] = data["wind_spd"]
    current_conds['last_call'] = time.time()

def get_forcast():

    url = "https://api.weatherbit.io/v2.0/forecast/daily?city=Weymouth&country=US&days=2&units=I&" \
          "key=1933cd653d1b4ea899c8a2bb89f14ead"

    response = requests.get(url)
    data = json.loads(response.content)
    pp = pprint.PrettyPrinter(indent=4)

    for day in data['data']:
        date = datetime.datetime.strptime(day['valid_date'], '%Y-%m-%d').date()
        tomorrow = datetime.date.today() + datetime.timedelta(days=1)
        if date == tomorrow:
            fore_conds['date'] = date.strftime('%m/%d')
            fore_conds['high_temp'] = int(round(day['high_temp']))
            fore_conds['low_temp'] = int(round(day['low_temp']))
            fore_conds['description'] = day['weather']['description']
            fore_conds['snow'] = round(day['snow_depth'], 3)
            fore_conds['precip'] = round(day['precip'], 3) # 1/100 inch of rain measurable by NWS
            fore_conds['humidity'] = int(round(day['rh']))
            fore_conds['wind_gust'] = int(day['wind_gust_spd'])
            fore_conds['wind_speed'] = int(round(day['wind_spd']))
            fore_conds['pop'] = int(day['pop'])
            fore_conds["wind_cdir"] = day['wind_cdir']
            fore_conds['last_call'] = time.time()

